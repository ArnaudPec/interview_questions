Top Embedded C programming Interview questions and answers for freshers and
experienced on embedded system concepts like RTOS, ISR, processors etc. with
best answers.


## 1) What is the use of volatile keyword?
<details>
<summary>Answer</summary>

The C's volatile keyword is a qualifier that tells the compiler not to optimize
when applied to a variable.  By declaring a variable volatile, we can tell the
compiler that the value of the variable may change any moment from outside of
the scope of the program.  A variable should be declared volatile whenever its
value could change unexpectedly and beyond the comprehension of the compiler.

In those cases it is required not to optimize the code, doing so may lead to
erroneous result and load the variable every time it is used in the program.
Volatile keyword is useful for memory-mapped peripheral registers, global
variables modified by an interrupt service routine, global variables accessed by
multiple tasks within a multi-threaded application.

</details>

## 2) Can a variable be both const and volatile?
<details><summary>Answer</summary>

The const keyword make sure that the value of the variable declared as const
can't be changed. This statement holds true in the scope of the program. The
value can still be changed by outside intervention. So, the use of const with
volatile keyword makes perfect sense.


</details>

## 3) Can a pointer be volatile?

<details><summary>Answer</summary>

If we see the declaration volatile int *p, it means that the pointer itself is
not volatile and points to an integer that is volatile. This is to inform the
compiler that pointer p is pointing to an integer and the value of that integer
may change unexpectedly even if there is no code indicating so in the program.

</details>

## 4) What is size of character, integer, integer pointer, character pointer?

<details><summary>Answer</summary>

    The sizeof character is 1 byte.
    Size of integer is 4 bytes.
    Size of integer pointer and character is 8 bytes on 64 bit machine and 4 bytes on 32 bit machine.


</details>

## 5) What is NULL pointer and what is its use?
<details><summary>Answer</summary>

The NULL is a macro defined in C. Null pointer actually means a pointer that
does not point to any valid location. We define a pointer to be null when we
want to make sure that the pointer does not point to any valid location and not
to use that pointer to change anything. If we don't use null pointer, then we
can't verify whether this pointer points to any valid location or not.


</details>

## 6) What is void pointer and what is its use?
<details><summary>Answer</summary>

The void pointer means that it points to a variable that can be of any type.
Other pointers points to a specific type of variable while void pointer is a
somewhat generic pointer and can be pointed to any data type, be it standard
data type(int, char etc) or user define data type (structure, union etc.). We
can pass any kind of pointer and reference it as a void pointer. But to
dereference it, we have to type the void pointer to correct data type.


</details>

## 7) What is ISR?
<details><summary>Answer</summary>

An ISR(Interrupt Service Routine) is an interrupt handler, a callback
subroutine which is called when a interrupt is encountered.


</details>

## 8) What is return type of ISR?
<details><summary>Answer</summary>

ISR does not return anything. An ISR returns nothing because there is no caller
in the code to read the returned values.


</details>

## 9) What is interrupt latency?
<details><summary>Answer</summary>

Interrupt latency is the time required for an ISR responds to an interrupt.


</details>

## 10) How to reduce interrupt latency?
<details><summary>Answer</summary>

Interrupt latency can be minimized by writing short ISR routine and by not
delaying interrupts for more time.

</details>

## 11) Can we use any function inside ISR?

<details><summary>Answer</summary>

We can use function inside ISR as long as
that function is not invoked from other portion of the code.

</details>



## 12) Can we use printf inside ISR?
<details><summary>Answer</summary>

Printf function in ISR is not supported because printf function is not
reentrant, thread safe and uses dynamic memory allocation which takes a lot of
time and can affect the speed of an ISR up to a great extent.


</details>

## 13) Can we put breakpoint inside ISR?
<details><summary>Answer</summary>

Putting a break point inside ISR is not a good idea because debugging will take
some time and a difference of half or more second will lead to different
behavior of hardware. To debug ISR, definitive logs are better.


</details>

## 14) Can static variables be declared in a header file?
<details><summary>Answer</summary>

A static variable cannot be declared without defining it. A static variable can
be defined in the header file. But doing so, the result will be having a
private copy of that variable in each source file which includes the header
file. So it will be wise not to declare a static variable in header file,
unless you are dealing with a different scenario.


</details>

## 15) Is Count Down_to_Zero Loop better than Count_Up_Loops?
<details><summary>Answer</summary>

Count down to zero loops are better. Reason behind this is that at loop
termination, comparison to zero can be optimized by the compiler. Most
processors have instruction for comparing to zero. So they don't need to load
the loop variable and the maximum value, subtract them and then compare to
zero. That is why count down to zero loop is better.


</details>

## 16) What are inline functions?
<details><summary>Answer</summary>

The ARM compilers support inline functions with the keyword "__inline". These
functions have a small definition and the function body is substituted in each
call to the inline function. The argument passing and stack maintenance is
skipped and it results in faster code execution, but it increases code size,
particularly if the inline function is large or one inline function is used
often.


</details>

## 17) Can include files be nested?
<details><summary>Answer</summary>

Yes. Include files can be nested any number of times. But you have to make sure
that you are not including the same file twice. There is no limit to how many
header files that can be included. But the number can be compiler dependent,
since including multiple header files may cause your computer to run out of
stack memory.


</details>

## 18) What are the uses of the keyword static?
<details><summary>Answer</summary>

Static keyword can be used with variables as well as functions. A variable
declared static will be of static storage class and within a function, it
maintains its value between calls to that function. A variable declared as
static within a file, scope of that variable will be within that file, but it
can't be accessed by other files.

Functions declared static within a module can be accessed by other functions
within that module. That is, the scope of the function is localized to the
module within which it is declared.


</details>

## 19) What are the uses of the keyword volatile?
<details><summary>Answer</summary>

Volatile keyword is used to prevent compiler to optimize a variable which can
change unexpectedly beyond compiler's comprehension. Suppose, we have a
variable which may be changed from scope out of the program, say by a signal,
we do not want the compiler to optimize it. Rather than optimizing that
variable, we want the compiler to load the variable every time it is
encountered. If we declare a variable volatile, compiler will not cache it in
its register.


</details>

## 20) What is Top half & bottom half of a kernel?
<details><summary>Answer</summary>

Sometimes to handle an interrupt, a substantial amount of work has to be done.
But it conflicts with the speed need for an interrupt handler. To handle this
situation, Linux splits the handler into two parts – Top half and Bottom half.
The top half is the routine that actually responds to the interrupt. The bottom
half on the other hand is a routine that is scheduled by the upper half to be
executed later at a safer time.

All interrupts are enabled during execution of the bottom half. The top half
saves the device data into the specific buffer, schedules bottom half and
exits. The bottom half does the rest. This way the top half can service a new
interrupt while the bottom half is working on the previous.


</details>

## 21) Difference between RISC and CISC processor.
<details><summary>Answer</summary>

* RISC (Reduced Instruction Set Computer) could carry out a few sets of simple
instructions simultaneously. Fewer transistors are used to manufacture RISC,
which makes RISC cheaper. RISC has uniform instruction set and those
instructions are also fewer in number. Due to the less number of instructions
as well as instructions being simple, the RISC computers are faster. RISC
emphasise on software rather than hardware. RISC can execute instructions in
one machine cycle.

* CISC (Complex Instruction Set Computer) is capable of executing multiple
operations through a single instruction. CISC have rich and complex instruction
set and more number of addressing modes. CISC emphasise on hardware rather that
software, making it costlier than RISC. It has a small code size, high cycles
per second and it is slower compared to RISC.


</details>

## 22) What is RTOS?
<details><summary>Answer</summary>

In an operating system, there is a module called the scheduler, which schedules
different tasks and determines when a process will execute on the processor.
This way, the multi-tasking is achieved. The scheduler in a Real Time Operating
System (RTOS) is designed to provide a predictable execution pattern. In an
embedded system, a certain event must be entertained in strictly defined time.
To meet real time requirements, the behaviour of the scheduler must be
predictable. This type of OS which have a scheduler with predictable execution
pattern is called Real Time OS(RTOS). The features of an RTOS are

    *Context switching latency should be short.

    * Interrupt latency should be short.

    * Interrupt dispatch latency should be short.

    * Reliable and time bound inter process mechanisms.

    * Should support kernel preemption.


</details>

## 23) What is the difference between hard real-time and soft real-time OS?
<details><summary>Answer</summary>

A Hard real-time system strictly adheres to the deadline associated with the
task. If the system fails to meet the deadline, even once, the system is
considered to have failed. In case of a soft real-time system, missing a
deadline is acceptable. In this type of system, a critical real-time task gets
priority over other tasks and retains that priority until it completes.


</details>

## 24) What type of scheduling is there in RTOS?
<details><summary>Answer</summary>

RTOS uses pre-emptive scheduling. In pre-emptive scheduling, the higher
priority task can interrupt a running process and the interrupted process will
be resumed later.


</details>

## 25) What is priority inversion?
<details><summary>Answer</summary>

If two tasks share a resource, the one with higher priority will run first.
However, if the lower-priority task is using the shared resource when the
higher-priority task becomes ready, then the higher-priority task must wait for
the lower-priority task to finish. In this scenario, even though the task has
higher priority it needs to wait for the completion of the lower-priority task
with the shared resource. This is called priority inversion.


</details>

## 26) What is priority inheritance?
<details><summary>Answer</summary>

Priority inheritance is a solution to the priority inversion problem. The
process waiting for any resource which has a resource lock will have the
maximum priority. This is priority inheritance. When one or more high priority
jobs are blocked by a job, the original priority assignment is ignored and
execution of critical section will be assigned to the job with the highest
priority in this elevated scenario. The job returns to the original priority
level soon after executing the critical section.


</details>

## 27) How many types of IPC mechanism you know?
<details><summary>Answer</summary>

Different types of IPC mechanism are -

    * Pipes

    * Named pipes or FIFO

    * Semaphores

    * Shared memory

    * Message queue

    * Socket


</details>

## 28) What is semaphore?
<details><summary>Answer</summary>

Semaphore is actually a variable or abstract data type which controls access to
a common resource by multiple processes. Semaphores are of two types -

* Binary semaphore : It can have only two values (0 and 1). The semaphore
value is set to 1 by the process in charge, when the resource is available.

* Counting semaphore : It can have value greater than one. It is used to
control access to a pool of resources.


</details>

## 29) What is spin lock?
<details><summary>Answer</summary>

If a resource is locked, a thread that wants to access that resource may
repetitively check whether the resource is available. During that time, the
thread may loop and check the resource without doing any useful work. Suck a
lock is termed as spin lock.


</details>

## 30) What is difference between binary semaphore and mutex?
<details><summary>Answer</summary>

The differences between binary semaphore and mutex are as follows

Mutual exclusion and synchronization can be used by binary semaphore while
mutex is used only for mutual exclusion.

A mutex can be released by the same thread which acquired it.

Semaphore values can be changed by other thread also.

From an ISR, a mutex can not be used.  The advantage of
semaphores is that, they can be used to synchronize two unrelated processes
trying to access the same resource.  Semaphores can act as mutex, but the
opposite is not possible.


</details>

## 31) What is virtual memory?
<details><summary>Answer</summary>

Virtual memory is a technique that allows processes to allocate memory in case
of physical memory shortage using automatic storage allocation upon a request.
The advantage of the virtual memory is that the program can have a larger
memory than the physical memory. It allows large virtual memory to be provided
when only a smaller physical memory is available. Virtual memory can be
implemented using paging.

A paging system is quite similar to a paging system with swapping. When we want
to execute a process, we swap it into memory. Here we use a lazy swapper called
pager rather than swapping the entire process into memory. When a process is to
be swapped in, the pager guesses which pages will be used based on some
algorithm, before the process is swapped out again. Instead of swapping whole
process, the pager brings only the necessary pages into memory. By that way, it
avoids reading in unnecessary memory pages, decreasing the swap time and the
amount of physical memory.


</details>

## 32) What is kernel paging?
<details><summary>Answer</summary>

Paging is a memory management scheme by which computers can store and retrieve
data from the secondary memory storage when needed in to primary memory. In
this scheme, the operating system retrieves data from secondary storage in
same-size blocks called pages. The paging scheme allows the physical address
space of a process to be non continuous. Paging allows OS to use secondary
storage for data that does not fit entirely into physical memory.


</details>

## 33) Can structures be passed to the functions by value?
<details><summary>Answer</summary>

Passing structure by its value to a function is possible, but not a good
programming practice. First of all, if we pass the structure by value and the
function changes some of those values, then the value change is not reflected
in caller function. Also, if the structure is big, then passing the structure
by value means copying the whole structure to the function argument stack which
can slow the program by a significant amount.


</details>

## 34) Why cannot arrays be passed by values to functions?
<details><summary>Answer</summary>

In C, the array name itself represents the address of the first element. So,
even if we pass the array name as argument, it will be passed as reference and
not its address.


</details>

## 35) Advantages and disadvantages of using macro and inline functions?
<details><summary>Answer</summary>

The advantage of the macro and inline function is that the overhead for
argument passing and stuff is reduced as the function are in-lined. The
advantage of macro function is that we can write type insensitive functions. It
is also the disadvantage of macro function as macro functions can't do
validation check. The macro and inline function also increases the size of the
executable.


</details>

## 36) What happens when recursive functions are declared inline?
<details><summary>Answer</summary>

Inlining an recursive function reduces the overhead of saving context on stack.
But, inline is merely a suggestion to the compiler and it does not guarantee
that a function will be inlined. Obviously, the compiler won't be able to
inline a recursive function infinitely. It may not inline it at all or it may
inline it, just a few levels deep.


</details>

## 37) #define cat(x,y) x##y concatenates x to y. But cat(cat(1,2),3) does not expand but gives preprocessor warning. Why?

<details><summary>Answer</summary>

The cat(x, y) expands to x##y. It just pastes x and y. But in case of
cat(cat(1,2),3), it expands to cat(1,2) ## 3 instead of 1## 2## 3. That is why it
is giving preprocessor warning.


</details>

## 38) ++*ip increments what?
<details><summary>Answer</summary>

It increments the value to which ip points to and not the address.


</details>

## 39) Declare a manifest constant that returns the number of seconds in a year
<details><summary>Answer</summary>
using preprocessor? Disregard leap years in your answer.

The correct answer will be

    #define SECONDS_IN_YEAR (60UL * 60UL * 24UL * 365UL)

Do not forget to use UL, since the output will be very big integer.


</details>

## 40) Using the variable a, write down definitions for the following:
<details><summary>Answer</summary>

    An integer
    A pointer to an integer
    A pointer to a pointer to an integer
    An array of ten integers
    An array of ten pointers to integers
    A pointer to an array of ten integers
    A pointer to a function that takes an integer as an argument and returns an integer
    Pass an array of ten pointers to a function that take an integer argument and return an integer.

The correct answer is as follows -

    int a;
    int *a;
    int **a;
    int a[10];
    int *a[10];
    int (*a)[10];
    int (*a)(int);
    int (*a[10])(int);

If you have problem understanding these, please read pointer section in the provide tutorial thoroughly.


</details>

## 41) Consider the two statements below and point out which one is preferred and why?
<details><summary>Answer</summary>

    #define B struct A *
    typedef struct A * C;

The typedef is preferred. Both statements declare pointer to struct A to
something else and in one glance both looks fine. But there is one issue with
the define statement. Consider a situation where we want to declare p1 and p2
as pointer to struct A. We can do this by -

    C p1, p2;

But doing - B p1, p2, it will be expanded to struct A * p1, p2. It means that
p1 is a pointer to struct A but p2 is a variable of struct A and not a pointer.


</details>

## 42) What will be the output of the following code fragment?
<details><summary>Answer</summary>

    char *ptr;
    if ((ptr = (char *)malloc(0)) == NULL)
    {
       puts("Got a null pointer");
    }
    else
    {
       puts("Got a valid pointer");
    }

The output will be “Got a valid pointer”. It is because malloc(0) returns a
valid pointer, but it allocates size 0. So this pointer is of no use, but we
can use this free pointer and the program will not crash.


</details>

## 43) What is purpose of keyword const?
<details><summary>Answer</summary>

The const keyword when used in c means that the value of the variable will not
be changed. But the value of the variable can be changed using a pointer. The
const identifier can be used like this -

    const int a; or int const a;

Both means the same and it indicates that a is an constant integer. But if we declare something like this -

    const int *p

then it does not mean that the pointer is constant but rather it is pointing to an constant integer. The declaration of an const pointer to a non-constant integer will look like this -

    int * cont p;


</details>

## 44) What do the following declarations mean?
<details><summary>Answer</summary>

    const int a;
    int const a;
    const int *a;
    int * const a;
    int const * a const;

    The first two means that a is a constant integer.
    The third declaration means that a is a pointer to a constant integer.
    The fourth means that a is a constant pointer to a non-constant integer.
    The fifth means that a is a constant pointer to a constant integer.


</details>

## 45) How to decide whether given processor is using little endian format or big endian format ?
<details><summary>Answer</summary>

The following program can find out the endianness of the processor.

    #include<stdio.h>
    main ()
    {
     union Test
     {
        unsigned int i;
        unsigned char c[2];
     };
     union Test a = {300};
     if((a.c [0] == 1) &&  (a.c [1] ==
     </details>

     ## 44))
     <details><summary>Answer</summary>
     {
        printf ("BIG ENDIAN\n");
     }
     else
     {
        printf ("LITTLE ENDIAN\n");
     }
    }


</details>

## 46) What is the concatenation operator?
<details><summary>Answer</summary>

The Concatenation operator (##) in
macro is used to concatenate two arguments. Literally, we can say that the
arguments are concatenated, but actually their value are not concatenated.
Think it this way, if we pass A and B to a macro which uses ## to concatenate
those two, then the result will be AB. Consider the example to clear the
confusion-

    #define SOME_MACRO(a, b) a##b
    main()
    {
      int var = 15;
      printf(“%d”, SOME_MACRO(v, ar));
    }

Output of the above program will be 15.

</details>

## 47) Infinite loops often arise in embedded systems. How does you code an infinite loop in C?
<details><summary>Answer</summary>
There are several ways to code an infinite loop -

    while(1)
    { }
    or,
    for(;;)
    { }
    or,
    Loop:
    goto Loop

But many programmers prefer the first solution as it is easy to read and self-explanatory, unlike the second or the last one.


</details>

## 48) Guess the output:
<details><summary>Answer</summary>

    main()
    {
     fork();
     fork();
     fork();
     printf("hello world\n");
    }

It will print “hello world' 8 times. The main() will print one time and creates
3 children, let us say Child_1, Child_2, Child_3. All of them printed once. The
Child_3 will not create any child. Child2 will create one child and that child
will print once. Child_1 will create two children, say Child_4 and Child_5 and
each of them will print once. Child_4 will again create another child and that
child will print one time. A total of eight times the printf statement will be
executed.


</details>

## 49) What is forward reference w.r.t. pointers in c?
<details><summary>Answer</summary>

Forward Referencing with respect to pointers is used when a pointer is declared
and compiler reserves the memory for the pointer, but the variable or data type
is not defined to which the pointer points to. For example

    struct A *p;
    struct A
    {
     // members
    };


</details>

## 50) How is generic list manipulation function written which accepts elements of any kind?
<details><summary>Answer</summary>

It can be achieved using void pointer. A list may be expressed by a structure as shown below

    typedef struct
    {
     node *next;
     /* data part */
     ......
    }node;

Assuming that the generic list may be like this

    typedef struct
    {
     node *next;
     void *data;
    }node;

This way, the generic manipulation function can work on this type of structures.


</details>

## 51) How can you define a structure with bit field members?
<details><summary>Answer</summary>
Bit field members can be declared as shown below

    struct A
    {
     char c1 : 3;
     char c2 : 4;
     char c3 : 1;
    };

Here c1, c2 and c3 are members of a structure with width 3, 4, and 1 bit respectively. The ':' indicates that they are bit fields and the following numbers indicates the width in bits.

</details>

## 52) How do you write a function which takes 2 arguments - a byte and a field in the byte and returns the value of the field in that byte?
<details><summary>Answer</summary>
The function will look like this -

    int GetFieldValue(int byte, int field )
    {
      byte = byte >> field;
      byte = byte & 0x01;
      return byte;
    }

The byte is right shifted exactly n times where n is same as the field value.
That way, our intended value ends up in the 0th bit position. "Bitwise And"
with 1 can get the intended value. The function then returns the intended
value.


</details>

## 53) Which parameters decide the size of data type for a processor ?
<details><summary>Answer</summary>

Actually, compiler is the one responsible for size of the data type. But it is true as long as OS allows that. If it is not allowable by OS, OS can force the size.


</details>

## 54) What is job of preprocessor, compiler, assembler and linker ?
<details><summary>Answer</summary>

The preprocessor commands are processed and expanded by the preprocessor before
actual compilation. After preprocessing, the compiler takes the output of the
preprocessor and the source code, and generates assembly code. Once compiler
completes its work, the assembler takes the assembly code and produces an
assembly listing with offsets and generate object files.

The linker combines object files or libraries and produces a single executable
file. It also resolves references to external symbols, assigns final addresses
to functions and variables, and revises code and data to reflect new addresses.


</details>

## 55) What is the difference between static linking and dynamic linking ?
<details><summary>Answer</summary>

In static linking, all the library modules used in the program are placed in
the final executable file making it larger in size. This is done by the linker.
If the modules used in the program are modified after linking, then
re-compilation is needed. The advantage of static linking is that the modules
are present in an executable file. We don't want to worry about compatibility
issues.

In case of dynamic linking, only the names of the module used are present in
the executable file and the actual linking is done at run time when the program
and the library modules both are present in the memory. That is why, the
executables are smaller in size. Modification of the library modules used does
not force re-compilation. But dynamic linking may face compatibility issues
with the library modules used.


</details>

## 56) What is the purpose of the preprocessor directive #error?
<details><summary>Answer</summary>

Preprocessor error is used to throw a error message during compile time. We can check the sanity of the make file and using debug options given below

    #ifndef DEBUG
    #ifndef RELEASE
    #error Include DEBUG or RELEASE in the makefile
    #endif
    #endif


</details>

## 57) On a certain project it is required to set an integer variable at the absolute address 0x67a9 to the value 0xaa55. The compiler is a pure ANSI compiler. Write code to accomplish this task.
<details><summary>Answer</summary>

This can be achieved by the following code fragment:

    int *ptr;
    ptr = (int *)0x67a9;
    *ptr = 0xaa55;


</details>

## 58) Significance of watchdog timer in Embedded Systems.
<details><summary>Answer</summary>

The watchdog timer is a timing device with a predefined time interval. During
that interval, some event may occur or else the device generates a time out
signal. It is used to reset to the original state whenever some inappropriate
events take place which can result in system malfunction. It is usually
operated by counter devices.


</details>

## 59) Why ++n executes faster than n+1?
<details><summary>Answer</summary>

The expression ++n requires a single machine instruction such as INR to carry
out the increment operation. In case of n+1, apart from INR, other instructions
are required to load the value of n. That is why ++n is faster.


</details>

## 60) What is wild pointer?
<details><summary>Answer</summary>

A pointer that is not initialized to any valid address or NULL is considered as wild pointer. Consider the following code fragment -

    int *p;
    *p = 20;

Here p is not initialized to any valid address and still we are trying to access the address. The p will get any garbage location and the next statement will corrupt that memory location.


</details>

## 61) What is dangling pointer?
<details><summary>Answer</summary>

If a pointer is de-allocated or freed and the pointer is not assigned to NULL,
then it may still contain that address and accessing the pointer means that we
are trying to access that location and it will give an error. This type of
pointer is called dangling pointer.


</details>

## 62) Write down the equivalent pointer
<details><summary>Answer</summary>
expression for referring the same element a[i][j][k][l] ?

We know that a[i] can be written as *(a+i). Same way, the array elements can be written like pointer expression as follows -

    a[i][j] == *(*(a+i)+j)
    a[i][j][k] == *(*(*(a+i)+j)+k)
    a[i][j][k][l] == *(*(*(*(a+i)+j)+k)+l)




</details>

## 63) Which bit wise operator is suitable for checking whether a particular bit is on or off?
<details><summary>Answer</summary>

"Bitwise And" (&) is used to check if any particular bit is set or not. To check whether 5'th bit is set we can write like this

    bit = (byte >> 4) & 0x01;

Here, shifting byte by 4 position means taking 5'th bit to first position and "Bitwise And" will get the value in 0 or 1.


</details>

## 64) When should we use register modifier?
<details><summary>Answer</summary>

The register modifier is used when a variable is expected to be heavily used and keeping it in the CPU’s registers will make the access faster.


</details>

## 65) Why doesn't the following statement work?
<details><summary>Answer</summary>

    char str[ ] = "Hello" ;
    strcat ( str, '!' ) ;

The string function strcat( ) concatenates two strings. But here the second argument is '!', a character and that is the reason why the code doesn't work. To make it work, the code should be changed like this:

    strcat ( str, "!" ) ;


</details>

## 66) Predict the output or error(s) for the following program:
<details><summary>Answer</summary>

    void main()
    {
       int const * p = 5;
       printf("%d",++(*p));
    }

The above program will result in compilation error stating “Cannot modify a constant value”. Here p is a pointer to a constant integer. But in the next statement, we are trying to modify the value of that constant integer. It is not permissible in C and that is why it will give a compilation error.


</details>

## 67)Guess the output:
<details><summary>Answer</summary>

    #include<stdio.h>
    main()
    {
     unsigned int a = 2;
     int b = -10;
     (a + b > 0)?puts("greater than 0"):puts("less than 1");
    }

Output - greater than 0

If you have guessed the answer wrong, then here is the explanation for you. The
a + b is -8, if we do the math. But here the addition is between different
integral types - one is unsigned int and another is int. So, all the operands
in this addition are promoted to unsigned integer type and b turns to a
positive number and eventually a big one. The outcome of the result is
obviously greater than 0 and hence, this is the output.


</details>

## 68) Write a code fragment to set and clear only bit 3 of an integer.
<details><summary>Answer</summary>

    #define BIT(n) (0x1 << n)
    int a;
    void SetBit3()
    {
       a |= BIT(3);
    }

    void ClearBit3()
    {
       a &= ~BIT(3);
    }


</details>

## 69) What is wrong with this code?
<details><summary>Answer</summary>

    int square(volatile int *p)
    {
       return *p * *p;
    }

The intention of the above code is to return the square of the integer pointed
by the pointer p. Since it is volatile, the value of the integer may have
changed suddenly and will result in something else which will looks like the
result of the multiplication of two different integers. To work as expected,
the code needs to be modified like this.

    int square(volatile int *p)
    {
       int a = *p;
       return a*a;
    }


</details>

## 70) Is the code fragment given below is correct? If so what is the output?
<details><summary>Answer</summary>

    int i = 2, j = 3, res; res = i+++j;

The above code is correct, but little bit confusing. It is better not to follow
this type of coding style. The compiler will interpret above statement as “res
= i++ + j”. So the res will get value 5 and i after this will be 3.



</details>

## 71) Using the #define statement, how would you declare a manifest constant that returns the number of seconds in a year? Disregard leap years in your answer.
<details><summary>Answer</summary>

    #define SECONDS_PER_YEAR (60UL * 60UL * 24UL * 365UL)


</details>

## 72) Write the ‘standard’ MIN macro. That is, a macro that takes two arguments and returns the smaller of the two arguments.
<details><summary>Answer</summary>

    #define MIN(A,B)       ((A) <=  (B) ? (A) : (B))

The purpose of this question is to test the following:

(a)    Basic knowledge of the #define directive as used in macros. This is important, because until the inline operator becomes part of standard C, macros are the only portable way of generating inline code. Inline code is often necessary in embedded systems in order to achieve the required performance level.

(b)    Knowledge of the ternary conditional operator.  This exists in C because it allows the compiler to potentially produce more optimal code than an if-then-else sequence. Given that performance is normally an issue in embedded systems, knowledge and use of this construct is important.

(c)    Understanding of the need to very carefully parenthesize arguments to macros.

(d)    I also use this question to start a discussion on the side effects of macros, e.g. what happens when you write code such as :

    least = MIN(*p++, b);


</details>

## 73) What is the purpose of the preprocessor directive #error?
<details><summary>Answer</summary>

Either you know the answer to this, or you don’t. If you don’t, then see reference 1. This question is very useful for differentiating between normal folks and the nerds. It’s only the nerds that actually read the appendices of C textbooks that find out about such things.  Of course, if you aren’t looking for a nerd, the candidate better hope she doesn’t know the answer.
Infinite Loops


</details>

## 74) Infinite loops often arise in embedded systems. How does one code an infinite loop in C?
<details><summary>Answer</summary>

There are several solutions to this question. My preferred solution is:

    while(1) {}

Another common construct is:

    for(;;) {}

Personally, I dislike this construct because the syntax doesn’t exactly spell out what is going on.  Thus, if a candidate gives this as a solution, I’ll use it as an opportunity to explore their rationale for doing so.  If their answer is basically – ‘I was taught to do it this way and I have never thought about it since’ – then it tells me something (bad) about them. Conversely, if they state that it’s the K&R preferred method and the only way to get an infinite loop passed Lint, then they score bonus points.

A third solution is to use a goto:

    Loop:

    goto Loop;

Candidates that propose this are either assembly language programmers (which is probably good), or else they are closet BASIC / FORTRAN programmers looking to get into a new field.
Data declarations


</details>

## 75) Using the variable a, write down definitions for the following:
<details><summary>Answer</summary>

(a) An integer

(b) A pointer to an integer

(c) A pointer to a pointer to an integer

(d) An array of ten integers

(e) An array of ten pointers to integers

(f) A pointer to an array of ten integers

(g) A pointer to a function that takes an integer as an argument and returns an integer

(h)    An array of ten pointers to functions that take an integer argument and return an integer.

The answers are:

(a)    int a;                 // An integer

(b)    int *a;               // A pointer to an integer

(c)    int **a;             // A pointer to a pointer to an integer

(d)    int a[10];          // An array of 10 integers

(e)    int *a[10];        // An array of 10 pointers to integers

(f)     int (*a)[10];     // A pointer to an array of 10 integers

(g)    int (*a)(int);     // A pointer to a function a that takes an integer argument and returns an integer

(h)    int (*a[10])(int); // An array of 10 pointers to functions that take an integer argument and return an integer

People often claim that a couple of these are the sorts of thing that one looks up in textbooks – and I agree. While writing this article, I consulted textbooks to ensure the syntax was correct. However, I expect to be asked this question (or something close to it) when in an interview situation. Consequently, I make sure I know the answers – at least for the few hours of the interview.  Candidates that don’t know the answers (or at least most of them) are simply unprepared for the interview.  If they can’t be prepared for the interview, what will they be prepared for?
Static


</details>

## 76) What are the uses of the keyword static?
<details><summary>Answer</summary>

This simple question is rarely answered completely.  Static has three distinct uses in C:

(a)    A variable declared static within the body of a function maintains its value between function invocations.

(b)    A variable declared static within a module[1], (but outside the body of a function) is accessible by all functions within that module. It is not accessible by functions within any other module.  That is, it is a localized global.

(c)    Functions declared static within a module may only be called by other functions within that module. That is, the scope of the function is localized to the module within which it is declared.

Most candidates get the first part correct.  A reasonable number get the second part correct, while a pitiful number understand answer (c).  This is a serious weakness in a candidate, since they obviously do not understand the importance and benefits of localizing the scope of both data and code.
Const


</details>

## 77) What does the keyword const mean?
<details><summary>Answer</summary>

As soon as the interviewee says ‘const means constant’, I know I’m dealing with
an amateur. Dan Saks has exhaustively covered const in the last year, such that
every reader of ESP should be extremely familiar with what const can and cannot
do for you. If you haven’t been reading that column, suffice it to say that
const means “read-only”.  Although this answer doesn’t really do the subject
justice, I’d accept it as a correct answer. (If you want the detailed answer,
then read Saks’ columns – carefully!).

If the candidate gets the answer correct, then I’ll ask him these supplemental questions:

What do the following incomplete[2] declarations mean?

    const int a;

    int const a;

    const int *a;

    int * const a;

    int const * a const;

The first two mean the same thing, namely a is a const (read-only) integer.  The third means a is a pointer to a const integer (i.e., the integer isn’t modifiable, but the pointer is). The fourth declares a to be a const pointer to an integer (i.e., the integer pointed to by a is modifiable, but the pointer is not). The final declaration declares a to be a const pointer to a const integer (i.e., neither the integer pointed to by a, nor the pointer itself may be modified).

If the candidate correctly answers these questions, I’ll be impressed.

Incidentally, one might wonder why I put so much emphasis on const, since it is very easy to write a correctly functioning program without ever using it.  There are several reasons:

(a)    The use of const conveys some very useful information to someone reading your code. In effect, declaring a parameter const tells the user about its intended usage.  If you spend a lot of time cleaning up the mess left by other people, then you’ll quickly learn to appreciate this extra piece of information. (Of course, programmers that use const, rarely leave a mess for others to clean up…)

(b)    const has the potential for generating tighter code by giving the optimizer some additional information.

(c)    Code that uses const liberally is inherently protected by the compiler against inadvertent coding constructs that result in parameters being changed that should not be.  In short, they tend to have fewer bugs.

Volatile


</details>

## 78) What does the keyword volatile mean? Give three different examples of its use.
<details><summary>Answer</summary>

A volatile variable is one that can change unexpectedly.  Consequently, the compiler can make no assumptions about the value of the variable.  In particular, the optimizer must be careful to reload the variable every time it is used instead of holding a copy in a register.  Examples of volatile variables are:

(a)    Hardware registers in peripherals (e.g., status registers)

(b)    Non-stack variables referenced within an interrupt service routine.

(c)    Variables shared by multiple tasks in a multi-threaded application.

If a candidate does not know the answer to this question, they aren’t hired.  I consider this the most fundamental question that distinguishes between a ‘C programmer’ and an ‘embedded systems programmer’.  Embedded folks deal with hardware, interrupts, RTOSes, and the like. All of these require volatile variables. Failure to understand the concept of volatile will lead to disaster.

On the (dubious) assumption that the interviewee gets this question correct, I like to probe a little deeper, to see if they really understand the full significance of volatile. In particular, I’ll ask them the following:

(a) Can a parameter be both const and volatile? Explain your answer.

(b) Can a pointer be volatile? Explain your answer.

(c) What is wrong with the following function?:

    int square(volatile int *ptr)

    {

    return *ptr * *ptr;

    }

The answers are as follows:

(a)    Yes. An example is a read only status register. It is volatile because it can change unexpectedly. It is const because the program should not attempt to modify it.

(b)    Yes. Although this is not very common. An example is when an interrupt service routine modifies a pointer to a buffer.

(c)    This one is wicked.  The intent of the code is to return the square of the value pointed to by *ptr. However, since *ptr points to a volatile parameter, the compiler will generate code that looks something like this:

    int square(volatile int *ptr)

    {

    int a,b;

    a = *ptr;

    b = *ptr;

    return a * b;

}

Since it is possible for the value of *ptr to change unexpectedly, it is possible for a and b to be different. Consequently, this code could return a number that is not a square!  The correct way to code this is:

    long square(volatile int *ptr)

    {

    int a;

    a = *ptr;

    return a * a;

    }

Bit Manipulation


</details>

## 79) Embedded systems always require the user to manipulate bits in registers or variables.
<details><summary>Answer</summary>
Given an integer variable a, write two code fragments.
The first should set bit 3 of a.
The second should clear bit 3 of a.
In both cases, the remaining bits should be unmodified.

These are the three basic responses to this question:

(a) No idea. The interviewee cannot have done any embedded systems work.

(b) Use bit fields.  Bit fields are right up there with trigraphs as the most brain-dead portion of C.  Bit fields are inherently non-portable across compilers, and as such guarantee that your code is not reusable.  I recently had the misfortune to look at a driver written by Infineon for one of their more complex communications chip.  It used bit fields, and was completely useless because my compiler implemented the bit fields the other way around. The moral – never let a non-embedded person anywhere near a real piece of hardware![3]

(c) Use #defines and bit masks.  This is a highly portable method, and is the one that should be used.  My optimal solution to this problem would be:

    #define BIT3       (0x1 << 3)
    static int a;
    void set_bit3(void) {
        a |= BIT3;
    }
    void clear_bit3(void) {
        a &= ~BIT3;
    }

Some people prefer to define a mask, together with manifest constants for the set & clear values.  This is also acceptable.  The important elements that I’m looking for are the use of manifest constants, together with the |= and &= ~ constructs.
Accessing fixed memory locations


</details>

## 80) Embedded systems are often characterized by requiring the programmer to access a specific memory location.
<details><summary>Answer</summary>
On a certain project it is required to set an integer variable at the absolute address 0x67a9 to the value 0xaa55.
The compiler is a pure ANSI compiler. Write code to accomplish this task.

This problem tests whether you know that it is legal to typecast an integer to a pointer in order to access an absolute location.  The exact syntax varies depending upon one’s style. However, I would typically be looking for something like this:

    int *ptr;

    ptr = (int *)0x67a9;

    *ptr = 0xaa55;

A more obfuscated approach is:

    *(int * const)(0x67a9) = 0xaa55;

Even if your taste runs more to the second solution, I suggest the first solution when you are in an interview situation.
Interrupts


</details>

## 81) Interrupts are an important part of embedded systems.
<details><summary>Answer</summary>
Consequently, many compiler vendors offer an extension to standard C to support interrupts.
Typically, this new key word is "__interrupt".
The following code uses "__interrupt" to define an interrupt service routine.
Comment on the code.

    __interrupt double compute_area(double radius) {
    {
        double area = PI * radius * radius;
        printf(“\nArea = %f”, area);
        return area;
    }

This function has so much wrong with it, it’s almost tough to know where to start.

(a)    Interrupt service routines cannot return a value. If you don’t understand this, then you aren’t hired.

(b)    ISR’s cannot be passed parameters. See item (a) for your employment prospects if you missed this.

(c)    On many processors / compilers, floating point operations are not necessarily re-entrant. In some cases one needs to stack additional registers, in other cases, one simply cannot do floating point in an ISR. Furthermore, given that a general rule of thumb is that ISRs should be short and sweet, one wonders about the wisdom of doing floating point math here.

(d)    In a similar vein to point (c), printf() often has problems with reentrancy and performance.  If you missed points (c) & (d) then I wouldn’t be too hard on you.  Needless to say, if you got these two points, then your employment prospects are looking better and better.
Code Examples


</details>

## 82) What does the following code output and why?
<details><summary>Answer</summary>

    void foo(void)

    {
        unsigned int a = 6;
        int b = -20;
        (a+b > 6) ? puts(“> 6”) : puts(“<= 6”);
    }

This question tests whether you understand the integer promotion rules in C – an area that I find is very poorly understood by many developers.
Anyway, the answer is that this outputs “> 6”.  The reason for this is that expressions involving signed and unsigned types have all operands promoted to unsigned types.
Thus –20 becomes a very large positive integer and the expression evaluates to greater than 6.
This is a very important point in embedded systems where unsigned data types should be used frequently (see reference 2).
{{{{If you get this one wrong, then you are perilously close to not being hired.


</details>

## 83) Comment on the following code fragment?
<details><summary>Answer</summary>

    unsigned int zero = 0;

    unsigned int compzero = 0xFFFF;       /*1’s complement of zero */

On machines where an int is not 16 bits, this will be incorrect. It should be coded:

    unsigned int compzero = ~0;

This question really gets to whether the candidate understands the importance of word length on a computer.  In my experience, good embedded programmers are critically aware of the underlying hardware and its limitations, whereas computer programmers tend to dismiss the hardware as a necessary annoyance.

By this stage, candidates are either completely demoralized – or they are on a roll and having a good time.  If it is obvious that the candidate isn’t very good, then the test is terminated at this point. However, if the candidate is doing well, then I throw in these supplemental questions.  These questions are hard, and I expect that only the very best candidates will do well on them. In posing these questions, I’m looking more at the way the candidate tackles the problems, rather than the answers. Anyway, have fun…

Dynamic memory allocation.


</details>

## 84) Although not as common as in non-embedded computers, embedded systems still do dynamically allocate memory from the heap.  What are the problems with dynamic memory allocation in embedded systems?
<details><summary>Answer</summary>

Here, I expect the user to mention memory fragmentation, problems with garbage collection, variable execution time, etc. This topic has been covered extensively in ESP, mainly by Plauger.  His explanations are far more insightful than anything I could offer here, so go and read those back issues! Having lulled the candidate into a sense of false security, I then offer up this tidbit:

What does the following code fragment output and why?

    char *ptr;

    if ((ptr = (char *)malloc(0)) == NULL) {

        puts(“Got a null pointer”);

    }

    else {

        puts(“Got a valid pointer”);

    }

This is a fun question.  I stumbled across this only recently, when a colleague of mine inadvertently passed a value of 0 to malloc, and got back a valid pointer! After doing some digging, I discovered that the result of malloc(0) is implementation defined, so that the correct answer is ‘it depends’. I use this to start a discussion on what the interviewee thinks is the correct thing for malloc to do.  Getting the right answer here is nowhere near as important as the way you approach the problem and the rationale for your decision.
Typedef


</details>

## 85) Typedef is frequently used in C to declare synonyms for pre-existing data
<details><summary>Answer</summary>
types.  It is also possible to use the preprocessor to do something similar.
For instance, consider the following code fragment:

    #define dPS  struct s *

    typedef  struct s * tPS;

The intent in both cases is to define dPS and tPS to be pointers to structure
s.  Which method (if any) is preferred and why?

This is a very subtle question, and anyone that gets it right (for the right
reason) is to be congratulated or condemned (“get a life” springs to mind). The
answer is the typedef is preferred. Consider the declarations:

    dPS p1,p2;

    tPS p3,p4;

The first expands to

    struct s * p1, p2;

which defines p1 to be a pointer to the structure and p2 to be an actual
structure, which is probably not what you wanted. The second example correctly
defines p3 & p4 to be pointers.  Obfuscated syntax


</details>

## 86) C allows some appalling constructs.  Is this construct legal, and if so what does this code do?
<details><summary>Answer</summary>

    int a = 5, b = 7, c;

    c = a+++b;

This question is intended to be a lighthearted end to the quiz, as, believe it
or not, this is perfectly legal syntax.  The question is how does the compiler
treat it? Those poor compiler writers actually debated this issue, and came up
with the “maximum munch” rule, which stipulates that the compiler should bite
off as big a (legal) chunk as it can.  Hence, this code is treated as:

    c = a++ + b;

Thus, after this code is executed, a = 6, b = 7 & c = 12;

If you knew the answer, or guessed correctly – then well done.  If you didn’t
know the answer then I would not consider this to be a problem.  I find the
biggest benefit of this question is that it is very good for stimulating
questions on coding styles, the value of code reviews and the benefits of using
lint.


</details>

## 87) In which stage the following code #include<stdio.h> gets replaced by the contents of the file stdio.h?
<details><summary>Answer</summary>

During preprocessing.
The preprocessor replace the line #include... with the system header file of that name. (the entire text replace
the include directive).
If the file does not exists the preprocessor flashes an error message.


</details>

## 88) What are the types of linkage?
<details><summary>Answer</summary>

External, Internal and None.


</details>

## 89) Size of double? Long double?
<details><summary>Answer</summary>

8, 10 bytes.


</details>

## 90) The equivalent pointer expression by using the array element a[i][j][k][2]
<details><summary>Answer</summary>

    *(*(*(*(a+i)+j)+k)+2)

</details>

1. Difference between linked list and array? when to use linked list?
2. What are dangling pointers? where to use them?
3. What is recursion? What actually happens during recursion? does the memory get stored on stack? what gets called and how does the program know from where to call? how does unwinding happen? explain with example?
4. what are structures and unions? when to use what? sizes ?
5. If you have 2 int and 1 char as parameters for struct and union, what is memory allocated for struct and union in 32 bit processor?
6. what is free()? how does free know how much memory to de-allocate?

Embedded systems questions:

1. mutexes and semaphores? what is the main difference between them? what is the difference between binary semaphore and mutex? how does locking happen in mutex?
2. what is trashing? what happens during trashing? what is excessive paging?
3. what is dynamic loading? what is static loading? when to use dynamic loading? what are the advantages? give an example when to use dynamic loading?

</details>

# Code fragments



## 91) Allocate 2D array + print + fill + free
<details><summary>Answer</summary>

    #include <stdio.h>
    #define N 10
    #include <stdlib.h>

    void fill(int **tab)
    {
        int val = 0;

        for (int i = 0; i < N; ++i) {
           for (int j = 0; j < N; ++j) {
               /*tab[i][j] = val++;*/
               *(*(tab +i)+j) = val++;
           }
        }
    }

    void print(int **tab)
    {
        for (int i = 0; i < N; ++i) {
           for (int j = 0; j < N; ++j) {
               printf("%2.d ", tab[i][j]);
           }
           printf("\n");
        }
    }

    int main() {
        int **tab;

        // Allocate memory
        tab = (int **) malloc(sizeof(int *) * N);
        for (int i = 0; i < N; ++i) {
            tab[i] = (int *) malloc(sizeof(int) * N);
        }

        // fill
        fill(tab);

        print(tab);

        // Free memory
        for (int i = 0; i < N; ++i) {
            free(tab[i]);
        }
        free(tab);

        return 0;

    }


</details>

## 92) Swap a, b without temp
<details><summary>Answer</summary>

    #include <stdio.h>
    #define N 10
    #include <stdlib.h>

    void swap(int *a, int *b)
    {
        *a = *a + *b;
        *b = *a - *b;
        *a = *a - *b;
    }

    int main(int argc, char *argv[])
    {
        int a, b;
        a = 5;
        b = 3;

        swap(&a, &b);
        printf("a=%d b=%d\n", a, b);

        return 0;
    }


</details>

## 93) Fork exec
<details><summary>Answer</summary>

    include <stdio.h>
    #include <stdlib.h>
    #include <unistd.h>
    #include <sys/types.h>
    #include <sys/wait.h>
    #include <errno.h>
    #include <string.h>

    int main(int argc, char *argv[])
    {

        int pid;
        int rc;
        int status;

        const char *psz_argv_add_ip[] = {"sleep", "10", NULL};
        pid = fork();

        if ((int)pid == -1)
            rc = -1;
        else if ((int)pid == 0)
        {
            execve("/bin/sleep", (char *const *)psz_argv_add_ip, NULL);
            printf("err\n");
            exit(-1);
        }
        else
        {
            waitpid((int)pid, &rc, 0);

            if (WIFEXITED(status))
                rc = WEXITSTATUS(status);
        }
        printf("%d\n", rc);

        return rc;
    }


</details>

## 94) Linked list simple
<details><summary>Answer</summary>

    #include <stdlib.h>

    typedef struct element element;
    struct element
    {
        int val;
        struct element *nxt;
    };

    typedef element* llist;

    int main(int argc, char **argv)
    {
        llist ma_liste1 = NULL;
        element *ma_liste2 = NULL;
        struct element *ma_liste3 = NULL;

        ajouterEnTete(ma_liste1, 4);

        return 0;
    }

    llist ajouterEnTete(llist liste, int valeur)
    {
        element* nouvelElement = malloc(sizeof(element));

        nouvelElement->val = valeur;

        nouvelElement->nxt = liste;

        /* On retourne la nouvelle liste, i.e. le pointeur sur le premier élément */
        return nouvelElement;
    }

    int nombreElements(llist liste)
    {
        /* Si la liste est vide, il y a 0 élément */
        if(liste == NULL)
            return 0;

        /* Sinon, il y a un élément (celui que l'on est en train de traiter)
        plus le nombre d'éléments contenus dans le reste de la liste */
        return nombreElements(liste->nxt)+1;
    }

    llist effacerListe(llist liste)
    {
        if(liste == NULL)
        {
            /* Si la liste est vide, il n'y a rien à effacer, on retourne
            une liste vide i.e. NULL */
            return NULL;
        }
        else
        {
            /* Sinon, on efface le premier élément et on retourne le reste de la
            liste effacée */
            element *tmp;
            tmp = liste->nxt;
            free(liste);
            return effacerListe(tmp);
        }
    }


</details>

## 95) Linked list double
<details><summary>Answer</summary>

    typedef struct element element;
    struct element
    {
        int val;
        struct element *nxt;
        struct element *prev;
    };


</details>

## 96) Func pointers
<details><summary>Answer</summary>

    #include <stdio.h>

    struct numbers {
       int n1;
       int n2;
    };
    typedef struct numbers numbers_t;

    void call(void (*func)(void *), void* data)
    {
        func(data);
    }

    void Print_Int(void *);
    void add(void* numbers);

    int main()
    {
       int a = 20;
       numbers_t n = { 3, 4 };

       call(Print_Int, &a);
       call(add, &n);

       return 0;
    }
    void Print_Int(void * a )
    {
       int b = *(int *) a;
       printf("%d\n", b);
    }

    void add(void* numbers)
    {
       int n1, n2;
       numbers_t n = *(numbers_t *) numbers;
       n1 = n.n1;
       n2 = n.n2;

       printf("%d\n", n1 + n2);
    }


</details>

## 98) Semaphore
<details><summary>Answer</summary>

    // gcc -Wall -Wextra -lrt -lpthread sem.c
    #include <stdio.h>
    #include <pthread.h>
    #include <semaphore.h>
    #include <unistd.h>

    sem_t mutex;

    void* thread(void* arg)
    {
        //wait
        sem_wait(&mutex);
        printf("\nEntered..\n");

        //critical section
        sleep(4);

        //signal
        printf("\nJust Exiting...\n");
        sem_post(&mutex);
    }


    int main()
    {
        sem_init(&mutex, 0, 1);
        pthread_t t1,t2;

        pthread_create(&t1,NULL,thread,NULL);

        sleep(2);

        pthread_create(&t2,NULL,thread,NULL);

        pthread_join(t1,NULL);
        pthread_join(t2,NULL);

        sem_destroy(&mutex);
        return 0;
    }


</details>

## 99) Mutexes
<details><summary>Answer</summary>

    #include<stdio.h>
    #include<string.h>
    #include<pthread.h>
    #include<stdlib.h>
    #include<unistd.h>

    int counter;
    pthread_t tid[2];
    pthread_mutex_t lock;

    void* count(void *arg)
    {
        unsigned long i = 0;

        pthread_mutex_lock(&lock);

        counter += 1;
        printf("\n Job %d started\n", counter);

        for(i=0; i<(0xFFFFFFFF);i++);

        printf("\n Job %d finished\n", counter);

        pthread_mutex_unlock(&lock);

        return NULL;
    }

    int main(void)
    {
        pthread_mutex_init(&lock, NULL);

        pthread_create(&(tid[0]), NULL, &count, NULL);
        pthread_create(&(tid[1]), NULL, &count, NULL);

        pthread_join(tid[0], NULL);
        pthread_join(tid[1], NULL);
        pthread_mutex_destroy(&lock);

        return 0;
    }


</details>

## 99) Pthreads
<details><summary>Answer</summary>

    #include <stdio.h>
    #include <stdlib.h>
    #include <pthread.h>

    void *thread_1(void *arg) {
        int *i = (int *) arg;
        (*i)++;

        // clean stop
        pthread_exit(NULL);
    }

    int main(void) {
        int i = 1;
        pthread_t thread1;

        printf("before th creation, i = %i.\n", i); // 1

        pthread_create(&thread1, NULL, thread_1, &i);
        pthread_join(thread1, NULL);

        printf("After th creation, i = %i.\n", i); // 2
        return EXIT_SUCCESS;
    }



</details>

## 101) Structure, Unions and sizeof (inc. bitfields, typedefs)
<details><summary>Answer</summary>

    #include <stdio.h>

    union test {
        int x;
        char y;
    };

    int main()
    {
        union test p1;
        p1.x = 65;

        // p2 is a pointer to union p1
        union test* p2 = &p1;

        // Accessing union members using pointer
        printf("%d %c", p2->x, p2->y); // 65 A

        printf("%d", sizeof(p1)); // 8

        return 0;
    }



</details>

## 102) Operators priority
<details><summary>Answer</summary>


</details>

## 103) Palindrome
<details><summary>Answer</summary>

    void isPalindrome(char str[])
    {
        // Start from leftmost and rightmost corners of str
        int l = 0;
        int h = strlen(str) - 1;

        // Keep comparing characters while they are same
        while (h > l)
        {
            if (str[l++] != str[h--])
            {
                printf("%s is Not Palindrome", str);
                return;
            }
        }
        printf("%s is palindrome", str);
    }


</details>

## 104) Pi Monte carlo
<details><summary>Answer</summary>

    #include <stdio.h>
    #include <stdlib.h>
    #include <time.h>

    #define N 10000000

    struct point_t {
        double x;
        double y;
    };
    typedef struct point_t Point;

    void gen_point(Point *p)
    {
        p->x = (double)rand()/(double)((unsigned)RAND_MAX+1);
        p->y = (double)rand()/(double)((unsigned)RAND_MAX+1);
    }

    int is_on_circle(Point p)
    {
        return ((p.x * p.x + p.y * p.y) <= 1) ? 1 : 0;
    }

    int main(int argc, char *argv[])
    {
        int i;
        int circle_points = 0;
        double pi;
        Point p;

        srand(time(NULL));

        for (i = 0; i < N; ++i) {
            gen_point(&p);
            if (is_on_circle(p)) circle_points++;

            // printf("%f %f\n", p.x, p.y);

        }

        pi = 4 * (double)circle_points/(double)N;
        printf("pi : %f\n", pi);

        return 0;
    }

</details>



## 1) Exception in Cpp
<details>
<summary>Answer</summary>


    #include <cstdlib>
    #include <iostream>
    #include <algorithm>
    #include <functional>
    #include <numeric>
    #include <string>
    #include <sstream>
    #include <vector>
    #include <cmath>
    #include <ctime>
    
    int main(int argc, char *argv[])
    {
        //double num1 = 0, num2 = 0;
        //std::cout << "Enter number 1 : ";
        //std::cin >> num1;
        //std::cout << "Enter number 2 : ";
        //std::cin  >> num2;
    
        //try {
            //if (num2 == 0) {
               //throw "Div by 0 is not possible";
            //}
            //else {
                //printf("%.1f / %.1f = %.2f", num1, num2, num1/num2);
            //}
        //}
        //catch (const char *exp){
            //std::cout << exp << "\n";
        //}
    
        try {
        
        }catch(std::exception &exp) {
        
        }
    
        catch(...){ //catch all
        }
    
        return 0;
    }

</details>

## 2) Tupples in Cpp
<details>
<summary>Answer</summary>

    #include <iostream>
    #include <tuple>
    
    int main()
    {
        auto user_info = std::make_tuple("M", "Chowdhury", 25);
    
        std::cout
            << std::get<0>(user_info)
            << std::get<1>(user_info)
            << std::get<2>(user_info) << std::endl;
    
        // cpp17
        auto [fname, lname, age] = user_info;
    
        std::cout
            << fname
            << lname
            << age << std::endl;
    
        // cpp17
        std::pair user = {"M", 25}; // std::pair<std::string, int> user = {"M", 25};  //cpp11
        std::cout
            << std::get<0>(user)
            << std::get<1>(user) << std::endl;
    
        std::tuple user2("M", "Chy", 25);
        std::cout
            << std::get<0>(user2)
            << std::get<1>(user2)
            << std::get<2>(user2) << std::endl;
    
        return 0;
    }
</details>

## 3) Iterators

<details>
<summary>Answer</summary>


    #include <cstdlib>
    #include <iostream>
    #include <vector>
    
    int main(void)
    {
        std::exit(1);
        std::vector<int> v = { 1, 2, 3, 4, 5};
    
        for (int value : v) {
            std::cout << value << std::endl;
        }
        return 0;
    }
</details>

## 4) OO

<details>
<summary>Answer</summary>

    #include <iostream>
    #include <vector>
    #include <memory>
    
    class Pizza
    {
        public:
            virtual void MakePizza() = 0;
    };
    
    class NYStyleCrust
    {
        public:
            std::string AddIngredient(){
                return " Crust so thin";
            }
    };
    
    class DeepDishCrust
    {
        public:
            std::string AddIngredient(){
                return " Deep dish";
            }
    };
    
    template <typename T>
    class LotsOfMeat: public T
    {
        public:
            std::string AddIngredient(){
                return " Lots of Random Meat, " + T::AddIngredient();
            }
    };
    
    template <typename T>
    class Vegan: public T
    {
        public:
            std::string AddIngredient(){
                return " Vegan Cheese, veggies" + T::AddIngredient();
            }
    };
    template <typename T>
    class MeatNYStyle: public T, public Pizza
    {
        public:
            void MakePizza(){
                std::cout << "Meat NY style Pizza: " <<
                    T::AddIngredient();
            }
    };
    
    template <typename T>
    class VeganDeepDish: public T, public Pizza
    {
        public:
            void MakePizza(){
                std::cout << "Vegan deep dish: " <<
                    T::AddIngredient();
            }
    };
    
    int main()
    {
        std::vector<std::unique_ptr<Pizza>> PizzaOrders;
        PizzaOrders.emplace_back(new MeatNYStyle<LotsOfMeat<NYStyleCrust>>());
        PizzaOrders.emplace_back(new VeganDeepDish<Vegan<DeepDishCrust>>());
    
        for (auto &pizza : PizzaOrders) {
            pizza->MakePizza();
        }
        return 0;
    }
</details>

## 5) Templates

<details>
<summary>Answer</summary>

    <summary>Answer</summary>
    
    #include <iostream>
    #include <iterator>
    
    template<typename T>
    void Times2(T val){
        std::cout << val << " *2 = " << val * 2 << "\n";
    }
    
    template<typename T>
    T Add(T val, T val2){
        return val + val2;
    }
    
    
    template<typename T>
    T Max(T val1, T val2) {
        return (val1 < val2) ? val2 : val2;
    }
    
    int main(int argc, char *argv[])
    {
        Times2(6);
        Times2(5.49);
    
        std::cout << Max(4, 7) << "\n";
        std::cout << Max('A', 'f') << "\n";
        return 0;
    }

</details>

## 6) Class members


    #include <iostream>
    
    using namespace std;
    
    class Box {
       public:
          double length;   // Length of a box
          double breadth;  // Breadth of a box
          double height;   // Height of a box
    };
    
    int main() {
       Box Box1;        // Declare Box1 of type Box
       Box Box2;        // Declare Box2 of type Box
       double volume = 0.0;     // Store the volume of a box here
     
       // box 1 specification
       Box1.height = 5.0; 
       Box1.length = 6.0; 
       Box1.breadth = 7.0;
    
       // box 2 specification
       Box2.height = 10.0;
       Box2.length = 12.0;
       Box2.breadth = 13.0;
       
       // volume of box 1
       volume = Box1.height * Box1.length * Box1.breadth;
       cout << "Volume of Box1 : " << volume <<endl;
    
       // volume of box 2
       volume = Box2.height * Box2.length * Box2.breadth;
       cout << "Volume of Box2 : " << volume <<endl;
       return 0;
    }
    
## 7) Class methods


#include <iostream>

    using namespace std;
    
    class Box {
       public:
          double length;         // Length of a box
          double breadth;        // Breadth of a box
          double height;         // Height of a box
    
          // Member functions declaration
          double getVolume(void);
          void setLength( double len );
          void setBreadth( double bre );
          void setHeight( double hei );
    };
    
    // Member functions definitions
    double Box::getVolume(void) {
       return length * breadth * height;
    }
    
    void Box::setLength( double len ) {
       length = len;
    }
    void Box::setBreadth( double bre ) {
       breadth = bre;
    }
    void Box::setHeight( double hei ) {
       height = hei;
    }
    
    // Main function for the program
    int main() {
       Box Box1;                // Declare Box1 of type Box
       Box Box2;                // Declare Box2 of type Box
       double volume = 0.0;     // Store the volume of a box here
     
       // box 1 specification
       Box1.setLength(6.0); 
       Box1.setBreadth(7.0); 
       Box1.setHeight(5.0);
    
       // box 2 specification
       Box2.setLength(12.0); 
       Box2.setBreadth(13.0); 
       Box2.setHeight(10.0);
    
       // volume of box 1
       volume = Box1.getVolume();
       cout << "Volume of Box1 : " << volume <<endl;
    
       // volume of box 2
       volume = Box2.getVolume();
       cout << "Volume of Box2 : " << volume <<endl;
       return 0;
    }

## 8) Constructor

    #include <iostream>
     
    using namespace std;
    class Line {
       public:
          void setLength( double len );
          double getLength( void );
          Line(double len);  // This is the constructor
     
       private:
          double length;
    };
     
    // Member functions definitions including constructor
    Line::Line( double len) {
       cout << "Object is being created, length = " << len << endl;
       length = len;
    }
    void Line::setLength( double len ) {
       length = len;
    }
    double Line::getLength( void ) {
       return length;
    }
    
    // Main function for the program
    int main() {
       Line line(10.0);
     
       // get initially set length.
       cout << "Length of line : " << line.getLength() <<endl;
       
       // set line length again
       line.setLength(6.0); 
       cout << "Length of line : " << line.getLength() <<endl;
     
       return 0;
    }

## 9) Destructor


    #include <iostream>
     
    using namespace std;
    class Line {
       public:
          void setLength( double len );
          double getLength( void );
          Line();   // This is the constructor declaration
          ~Line();  // This is the destructor: declaration
     
       private:
          double length;
    };
     
    // Member functions definitions including constructor
    Line::Line(void) {
       cout << "Object is being created" << endl;
    }
    Line::~Line(void) {
       cout << "Object is being deleted" << endl;
    }
    void Line::setLength( double len ) {
       length = len;
    }
    double Line::getLength( void ) {
       return length;
    }
    
    // Main function for the program
    int main() {
       Line line;
     
       // set line length
       line.setLength(6.0); 
       cout << "Length of line : " << line.getLength() <<endl;
     
       return 0;
    }

## 10) Inheritance

    #include <iostream>
     
    using namespace std;
    
    // Base class
    class Shape {
       public:
          void setWidth(int w) {
             width = w;
          }
          void setHeight(int h) {
             height = h;
          }
          
       protected:
          int width;
          int height;
    };
    
    // Derived class
    class Rectangle: public Shape {
       public:
          int getArea() { 
             return (width * height); 
          }
    };
    
    int main(void) {
       Rectangle Rect;
     
       Rect.setWidth(5);
       Rect.setHeight(7);
    
       // Print the area of the object.
       cout << "Total area: " << Rect.getArea() << endl;
    
       return 0;
    }
